실행
----

 * 도움말: `$ ./index.js --help`
 * 테스트: `$ ./index.js`
 * 실제 의원에게 메일 발송: `$./index.js --live`

참고
----

 * 테스트 시에는 send 테이블에 로깅을 하지 않습니다.
 * 테스트 시에는 전체 메일 발송 대상 중 1건만 발송합니다.
 * 테스트 시에는 실제 의원 메일 대신 index.js 파일 내에 `TEST_EMAIL` 변수로 지정된 이메일로 발송합니다.
