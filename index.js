#!/usr/bin/env node

const sgMail = require('@sendgrid/mail');
const mysql = require('mysql');
const Promise = require('bluebird');
const _ = require('lodash');
const ejs = require('ejs');
const optparse = require('optparse');
const md5 = require('md5');

let is_live = false;


const parser = new optparse.OptionParser([
  [ '--help', 'shows help sections' ],
  [ '--live', '실제 의원에게 메시지 발송. live 옵션이 생략되면 TEST_EMAIL로 1건만 발송하고 send 테이블에 발송 기록을 남기지 않습니다.' ],
]);

parser.on('help', () => {
  console.info(parser.toString());
  process.exit(0);
});
parser.on('live', () => { is_live = true; });
parser.parse(process.argv);

console.info(`RUN AS ${is_live && 'LIVE' || 'TEST'} MODE`);


const TEST_EMAIL = 'devnoff@gmail.com';
const SENDGRID_API_KEY = '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';
let MYSQL_CONFIG = {
  host: 'sotong.cyhgt3qgb1z2.ap-northeast-2.rds.amazonaws.com',
  user: 'dev_app',
  password: 'sotongsotong',
  database: 'sotong',
  timezone: '+09:00'
};

if (!is_live) {
  MYSQL_CONFIG = {
    host: 'localhost',
    user: 'dev_app',
    password: 'sotongsotong',
    database: 'sotong',
    timezone: '+00:00'
  };
}

sgMail.setApiKey(SENDGRID_API_KEY);


const conn = mysql.createConnection(MYSQL_CONFIG);
conn.connect();

const query = ({ sql, args = [] }) => {
  return new Promise( (resolve, reject) => {
    conn.query(sql, args, (err, results) => {
      if (err) return reject(err);
      return resolve(results);
    });
  });
};

const readMaxSentUserPetitionId = () => {
  return query({
    sql: `
      SELECT MAX(to_user_petition_id) as max_value
      FROM sotong.send

    `,
  }).then( (rows) => rows && rows[0].max_value || 0 );
};

const readUserPetitionsList = ({ from_user_petition_id }) => {
  return query({
    sql: `
      SELECT p.id, p.congressman_id, p.message,
        ifnull(u.name, ''), u.age, u.sex, u.district, u.job, if(isnull(facebook_id), 'Naver', 'Facebook') as referrer
      FROM sotong.user_petition p
      LEFT JOIN sotong.user u ON p.user_id = u.id
      WHERE p.id > ?
      ORDER BY p.created DESC
    `,
    args: [ from_user_petition_id ],
  }).then( (rows) => {
    const data = {};
    rows.forEach( (row) => {
      if (!data[row.congressman_id]) data[row.congressman_id] = [];
      data[row.congressman_id].push(row);
    });
    return data;
  });
};

const readTotalUserPetitions = () => {
  return query({
    sql: `
      SELECT COUNT(*) AS cnt FROM sotong.user_petition
    `,
  }).then( (rows) => rows[0].cnt );
};

const readAllCongressmen = () => {
  return Promise.all([
    query({
      sql: `
        SELECT id, name, email, 0 AS total_user_petitions
        FROM sotong.congressman
      `,
    }),
    query({
      sql: `
        SELECT congressman_id, COUNT(*) AS cnt
        FROM sotong.user_petition
        GROUP BY congressman_id
      `,
    }),
  ]).then( ([ rows, rows2 ]) => {
    const data = {};
    rows.forEach( (row) => {
      data[row.id] = row;
    });
    rows2.forEach( (row) => {
      if (data[row.congressman_id])
        data[row.congressman_id].total_user_petitions = row.cnt;
    });
    return data;
  });
};

const send = ({ congressman, user_petitions, total_user_petitions }) => {
  // 토큰 키 생성
  const token_key = md5(Math.random());

  return new Promise( (resolve, reject) => {
    ejs.renderFile(
      './tmpl/v1.ejs',
      { congressman, user_petitions, total_user_petitions, token_key },
      {},
      (err, str) => {
        if (err) return reject(err);
        return resolve(str);
      }
    );
  }).then( (mail_content) => {
    const msg = {
      from: 'hello@sotong.co',
      to: is_live && congressman.email || TEST_EMAIL,
      bcc: 'noff@naver.com',
      subject: `[국회소통] ${congressman.name}의원 님께 도착한 새로운 청원이 있습니다.`,
      html: mail_content,
    };
    return sgMail.send(msg);
  }).then( (res) => {
    // 테스트 발송인 경우 send 테이블에 로그를 남기지 않음
    // if (!is_live) return null;
    const message_id = res[0].headers['x-message-id'];

    return query({
      sql: `INSERT INTO sotong.send SET ?`,
      args: [{
        congressman_id: congressman.id,
        petition_id: 1,
        from_user_petition_id: _.minBy(user_petitions, 'id').id,
        to_user_petition_id: _.maxBy(user_petitions, 'id').id,
        email: congressman.email,
        date_sent: new Date(),
        created: new Date(),
        token_key: token_key,
        message_id: message_id
      }],
    });
  });
};

const run = () => {
  const data = {
    congressmen: {},
    total_user_petitions: 0,
    user_petitions_list: {},
  };

  return Promise.all([
    readAllCongressmen(),
    readMaxSentUserPetitionId(),
    readTotalUserPetitions(),
  ]).then( ([ congressmen, max_sent_user_petition_id, total_user_petitions ]) => {
    // console.log(max_sent_user_petition_id, 'max_sent_user_petition_id');
    data.congressmen = congressmen;
    data.total_user_petitions = total_user_petitions;
    return readUserPetitionsList({
      from_user_petition_id: max_sent_user_petition_id,
    });
  }).then( (user_petitions_list) => {
    // 테스트 발송인 경우 1건만 sampling
    if (!is_live && !_.isEmpty(user_petitions_list)) {
      const k = _.keys(user_petitions_list)[0];
      user_petitions_list = {
        [k]: user_petitions_list[k],
      };
    }
    // console.log(user_petitions_list, 'user_petitions_list');

    data.user_petitions_list = user_petitions_list;
    return Promise.all( _.map(user_petitions_list, (user_petitions, congressman_id) => {
      const c = data.congressmen[congressman_id];
      console.info(`SEND ${c.name}(${is_live && c.email || TEST_EMAIL}) ${user_petitions.length} user petitions`);
      return send({
        congressman: c,
        user_petitions,
        total_user_petitions: data.total_user_petitions,
      });
    }), { concurrency: 5 } );
  }).then( () => {
    console.info(`DONE: ${_.keys(data.user_petitions_list).length}명의 의원에게 메일 발송 완료`);
    process.exit(0);
  }).catch( (err) => {
    console.error(err);
    process.exit(1);
  });
};

run();
