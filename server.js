const ejs = require('ejs');
const express = require('express');
const app = express();
const port = process.env.PORT || 5002;
const fs = require('fs');
const md5 = require('md5');

const congressman = {
  "id": "55",
  "name": "김영호",
  "district": "서울 서대문구을",
  "email": "kim-youngho@naver.com",
  "phone": "02-784-4020~2",
  "homepage": "http://blog.naver.com/frencisco",
  "url": "http://www.assembly.go.kr/assm/memPop/memPopup.do?dept_cd=9770934",
  "facebook": "https://www.facebook.com/stookim",
  "twitter": "https://twitter.com/minjoo05",
  "office": "",
  "fax": "",
  "total_user_petitions": 2,
  "party": {
      "id": 1,
      "name": "더불어민주당"
  }
};

const user_petitions = [
  {
    "id": "15",
    "congressman_id": "10",
    "name": "박용남",
    "user_id": "5",
    "petition_id": "1",
    "message": "신속한 처리 부탁드립니다!",
    "created": "2018-04-21 15:57:10",
    "petition_answer_id": null,
    "age": "30대",
    "sex": "남성",
    "job": "주부",
    "district": "서울 강남구",
    "referrer": "Naver"
  },
  {
    "id": "15",
    "congressman_id": "10",
    "name": "박용남",
    "user_id": "5",
    "petition_id": "1",
    "message": "6월 개헌을 바랍니다. 신속히 처리해주세요.",
    "created": "2018-04-21 15:57:10",
    "petition_answer_id": null,
    "age": "20대",
    "sex": "여성",
    "job": "직장인",
    "district": "서울 영등포구",
    "referrer": "Facebook"
  }
]

const total_user_petitions = 1;

const token_key = md5(Math.random());


app.get('/', (req, res) => {
  res.status(200).send(
    ejs.render(
      fs.readFileSync(__dirname + '/tmpl/v1.ejs', 'utf8'), 
      { congressman, user_petitions, total_user_petitions, token_key }, 
      {})
  );
})

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});
